﻿using System.Collections.Generic;

namespace GIZ.Model
{
    public class Graph
    {
        public Graph()
        {
            Nodes = new List<Node>();
        }

        public ICollection<Node> Nodes { get; set; }
    }
}