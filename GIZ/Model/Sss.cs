﻿using System.Collections.Generic;

namespace GIZ.Model
{
    public class Sss
    {
        public Sss()
        {
            Nodes = new List<Node>();
            Neighbours = new List<Sss>();
        }

        public List<Node> Nodes { get; set; }

        public List<Sss> Neighbours { get; set; }
    }
}