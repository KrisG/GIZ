﻿using System.Collections.Generic;

namespace GIZ.Model
{
    public class Node
    {
        public Node(int value)
        {
            Neighbours = new List<Node>();
            IsVisited = false;
            Value = value;
        }

        public int Value { get; set; }

        public bool IsVisited { get; set; }

        public ICollection<Node> Neighbours { get; set; }
    }
}