﻿using System;
using System.Collections.Generic;
using System.Linq;
using GIZ.Model;

namespace GIZ
{
    public class KosarajuHandler
    {
        public void DfsStack(Node n, Stack<Node> stack, bool[] visitControl)
        {
            visitControl[n.Value - 1] = true;

            foreach (var neighbour in n.Neighbours)
            {
                if (!visitControl[neighbour.Value - 1])
                {
                    DfsStack(neighbour, stack, visitControl);
                }
            }

            stack.Push(n);
        }

        public void DfsSpirint(Node n, bool[] visitControl, Sss sss)
        {
            visitControl[n.Value - 1] = true;
            
           sss.Nodes.Add(n);

            foreach (var neighbour in n.Neighbours)
            {
                if (!visitControl[neighbour.Value-1])
                {
                    DfsSpirint(neighbour, visitControl, sss);
                }
            }
        }

        public Stack<Node> Transpone(Graph graf, Stack<Node> stack)
        {
            var cleanList = new List<Node>();

            foreach (var node in graf.Nodes)
            {
                cleanList.Add(new Node(node.Value));
            }

            foreach (var node in graf.Nodes)
            {
                foreach (var neighbour in node.Neighbours)
                {
                    cleanList.ElementAt(neighbour.Value - 1).Neighbours.Add(cleanList.FirstOrDefault(x => x.Value == node.Value));
                }
            }

            var tempStack = new Stack<Node>();

            for (var i = stack.Count - 1; i >= 0; i--)
            {
                tempStack.Push(cleanList.FirstOrDefault(x => x.Value == stack.ElementAt(i).Value));
            }

            return tempStack;
        }

        public void PrintSss(Sss sss)
        {
            Console.WriteLine();
            Console.Write("SSS: ");
            foreach (var node in sss.Nodes)
            {
                Console.Write(node.Value + " ");
            }
        }
    }
}