﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GIZ.Model;

namespace GIZ
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Zmienne

            var graf = new Graph();
            var kosarajuHandler = new KosarajuHandler();
            var nodesStack = new Stack<Node>();

            #endregion

            #region Tworzenie grafu - inicjalizacja

            var grapgCount = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < grapgCount; i++)
            {
                graf.Nodes.Add(new Node(i + 1));
            }

            #endregion

            #region Ladowanie danych i tworenie grafu

            for (var i = 0; i < grapgCount; i++)
            {
                var line = Console.ReadLine();

                if (string.IsNullOrEmpty(line))
                {
                    throw new ArgumentException("Wiersz nie moze byc pusty!",
                        new NullReferenceException("Dane wierzecholka grafu zostaly nieuzupalnione"));
                }

                var neighbours = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    for (var j = 1; j < neighbours.Length; j++)
                    {
                        graf.Nodes.ElementAt(i).Neighbours.Add(graf.Nodes.ElementAt(Convert.ToInt32(neighbours[j]) - 1)); //dodanie do node'a referencji do jego sasiada.
                    }
                }
                catch (Exception)
                {
                    throw new ArgumentException("Wprowadzone dane sa niepoprawne!", new NullReferenceException("Dane wierzecholka grafu zostaly nieuzupalnione"));
                }
            }

            #endregion

            #region Kosaraju pierwszy dfs

            var visited = new bool[grapgCount];

            foreach (var node in graf.Nodes)
            {
                if (visited[node.Value - 1]) continue;

                kosarajuHandler.DfsStack(node, nodesStack, visited);
            }

            #endregion

            #region Druga faza kosaraju transponowanie + drugi dfs

            var transponedStack = kosarajuHandler.Transpone(graf, nodesStack);

            visited = new bool[grapgCount];

            var sssCollection = new List<Sss>();

            while (transponedStack.Count > 0)
            {
                var tempNode = transponedStack.Pop();

                if (visited[tempNode.Value - 1]) continue;

                var newSss = new Sss();
                sssCollection.Add(newSss);

                kosarajuHandler.DfsSpirint(tempNode, visited, sssCollection.FirstOrDefault(x => x == newSss));
            }

            #endregion

            #region Wypisanie SSS

            foreach (var sss in sssCollection)
            {
                kosarajuHandler.PrintSss(sss);
            }

            #endregion

            #region Znajdowanie sasiadow Sss

            foreach (var node in graf.Nodes)
            {
                var nodesSss = sssCollection.FirstOrDefault(x => x.Nodes.Any(y => y.Value == node.Value));

                foreach (var neighbour in node.Neighbours)
                {
                    if (nodesSss.Nodes.Any(x => x.Value == neighbour.Value)) continue;

                    var potentialNeighbour = sssCollection.FirstOrDefault(x => x.Nodes.Any(y => y.Value == neighbour.Value));

                    if (nodesSss.Neighbours.Contains(potentialNeighbour)) continue;

                    nodesSss.Neighbours.Add(potentialNeighbour);
                    potentialNeighbour.Neighbours.Add(nodesSss);
                }
            }

            #endregion

            #region Sortowanie kolekcji SSS i wypisanie wyniku

            sssCollection.Sort((x, y) => y.Neighbours.Count.CompareTo(x.Neighbours.Count));

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Wynik: ");
            kosarajuHandler.PrintSss(sssCollection.First());

            #endregion

            Console.ReadLine();
        }
    }
}
